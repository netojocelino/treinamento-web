## DIA 1
### HTML5 e CSS3

 Escrever esqueleto do site, entendendo toda a estrutura das tecnologias utilizadas.

Entender tanto o HTML quando o CSS, passo a passo, visando sempre compreender o que cada uma das tags significa e o impacto.


##### HTML5

 Criar estrutura básica, sem estilização, fazendo Header, Corpo, Footer.

Estrutura básica necessária:

 - Cabeçalho:

     - Marca do site,
  
     - Menu de navegação,

     - Formulário de pesquisa,

     - Link para redes sociais;

 - Corpo:
   - Shows mais bem votados,
   - Shows mais recentes,
   - Propaganda;
   - ***Observação***
     - Cada show precisa ter as informações de:
       - Nome,
       - Imagem,
       - Nota;
     - **Responsividade é extra, mas é necessário ter o mínimo de adaptação para telas menores.**
 - Rodapé
   - Menu de navegação,
   - Redes sociais


##### CSS

 Estilização da estrutura HTML, utilizando as principais regras do CSS.

Será adicionado ao projeto as seguintes modificações:
 - Cabeçalho:
   - Adicionar um espaçamento no menu de navegação
   - Mudar a cor do background do menu de navegação
   - Modificar a fonte e cor do menu de navegação

 - Corpo:
   - Alterar a fonte do nome de cada show
   - Fazer com que a imagem do show seja circular
   - Adicionar um efeito de sombra em cada show
 
 - Rodapé:
   - Adicionar ícones para cada rede social, cada um cor a cor da respectiva rede.
 

