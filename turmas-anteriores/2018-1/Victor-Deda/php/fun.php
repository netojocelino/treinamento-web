<?php

class Autor{
  public function toString(){
    return "Victor: victordeda@email.com";
  }

}


class Mat extends Autor{


  public function fatorial($i){
    if($i == 1){
      return 1;
    }
    return $i * $this->fatorial($i - 1);
  }

  public function fibonnaci($i){
    if($i==0) return 0;
    if($i==1) return 1;
    return $this->fibonnaci($i-2) + $this->fibonnaci($i - 1);
  }
}



$fib = $_GET['fibonnaci'];
$fat = $_GET['fatorial'];


$contas = new Mat;
echo $contas->toString();

if(isset($fib))
  echo $contas->fibonnaci($_GET["fibonnaci"]) . "<br>";
if(isset($fat))
  echo $contas->fatorial($_GET["fatorial"]) . "<br>";


?>
