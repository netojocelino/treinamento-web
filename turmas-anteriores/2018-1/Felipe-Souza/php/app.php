<?php
// header("content-type: json");
class metadados {
  private $titulo;

  function __construct($titulo){
    $this->titulo = $titulo;
  }

  public function getTitulo()
  {
    return $this->titulo;
  }

  public function setTitulo($titulo)
  {
    $this->titulo = $titulo;
  }
}

$md0 = new metadados("Como ser ninja");
$md1 = new metadados("Como ser atirador");
$md2 = new metadados("PHP MAstwer");



echo $md0->getTitulo()."<br>";
echo $md1->getTitulo()."<br>";
echo $md2->getTitulo()."<br>";

$md2->setTitulo("como matar zumbis");

echo $md2->getTitulo();

/**
Metadados
   título
Postagem <- Metadados
  Comentário
Página <- Metadados
  Informação

*/
 ?>
