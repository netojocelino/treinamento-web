Real Name
Jennifer "Jen" Walters

Powers
Like her cousin, Jennifer possesses great strength, durability, endurance and a healing factor. Unlike her cousin, she almost always retains her full intelligence and personality as She-Hulk. She is also able to switch from She-Hulk to Jennifer Walters, but does not always have control of her transformation. In the past, Jennifer's inability to switch forms was due to a psychological block. Jennifer has learned a Ovid body-switching technique, but rarely uses it.The Scarlet Witch (Wanda Maximoff), Scarlet Witch also put an enchantment on She-Hulk so that anyone who wishes to harm She-Hulk will not be able to perceive her while she's in the form of Jen Walters. While useful it later backfired and was removed by Doctor Strange (Stephen Strange), Doctor Strange.

Group Affiliations
Lady Liberators, employee of Goodman, Lieber, Kurtzberg, & Holliway; formerly Defenders, Avengers, Brides of Set, Fantastic Four, Fantastic Force, Heroes for Hire

