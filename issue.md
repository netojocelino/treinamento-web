# Sobre issues


Essa seção servirá basicamente para repotar erros ou discutir acerca de dúvidas ou erros que podem vir a ocorrer.

Além disso, este recurso deve servir como meio de comunicação mais aberta e transparente sobre dúvidas e tecnologias utilizadas na SofTeam.

  > ## *Aviso legal* 
  > ----------------
  > As opiniões aqui publicadas são de total 
  > responsabilidade dos seus autores, não refletindo 
  > necessariamente o posicionamento da organização.



## Como participar

O repositório `treinamento-web` foi criado para documentar e passar algum direcionamento para quem está entrando na empresa, direcionando cada membro para assuntos relevantes para serem estudados. Daí veio a necessidade de abrir um canal para que todos os membros tivessem um canal para discutir e tirar dúvidas de forma mais aberta e que possa ser utilizada futuramente para quem possuir as mesmas dúvidas.

Para isso basta abrir a aba `issues` e postar quaisquer dúvida, sugestões ou problemas, para que possam ser discutidas de forma bem aberta e saudável. 

Sinta-se à vontade para criar issues ou comentar como quiser, desde que antes utilize a busca nas issues para evitar posts duplicados.

![Exemplo de Issue](issue.png)


### Exemplo de ISSUE

Quando for criar um ISSUE deve seguir a seguinte estrutura, no título precisa estar entre colchetes a tecnologia que deve ser discutida, seguida do título da dúvida, e na descrição a dúvida em si, tentando ao máximo deixar ela sucinta.




## Regrinhas

  O objetivo aqui é ajudar as pessoas! Não há necessidade de impor como deve funcionar, pois deve ser gerido por todos os envolvidos, porém todas as discussões precisam ser saudaveis. Respeite o próximo.