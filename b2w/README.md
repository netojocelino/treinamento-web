# Nivelamento Portfolio #

 > Este desafio foi inteiramente baseado no desafio da [b2w marketplace][B2W Link].
 
 
## Sobre o desafio  ##
-----------------------
Para avaliar o n�vel de conhecimento, este desafio ir� servir para avaliar:

  - Conhecimento nas tecnologias utilizadas;
  
    - Git
	
	- HTML
	
	- CSS
	
	- JavaScript

	
  - Organiza��o e desempenho em atividades em curto prazo;
  

  
## Desafio ##

> Ser� necess�rio criar uma branch com o nome `portfolio-{USU�RIO_BITBUCKET}`, e dentro dela uma pasta com mesmo nome, onde ser� feito todo o fluxo de altera��es do projeto.

Ser� necess�rio desenvolver uma p�gina web que possa ser acessada independente do dispositivo utilizado.

As informa��es para ser utilizadas no projeto podem ser acessadas a partir [deste link][API Link], ficando a criterio do desenvolvedor utilizar os dados consumindo a API ou copiando as informa��es.

Uso de frameworks e bibliotecas ser�o de uso opcional.

Layout a ser seguido pode ser [acessado aqui](https://bitbucket.org/netojocelino/treinamento-web/src/master/b2w/img/layout.jpg) ou ser visualizado logo abaixo.
![layout](/b2w/img/layout.jpg)


 
##  Links �teis ##

[GitFLow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) - Fluxo de como se comportar no git.


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [B2W Link]: <https://github.com/b2w-marketplace/code-challenge>
   [API Link]: <http://www.mocky.io/v2/5d27421d320000492a71ba42>
   [GitFlow] : <https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow>
   [layout]  : <https://bitbucket.org/netojocelino/treinamento-web/src/master/b2w/img/layout.jpg>

