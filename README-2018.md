# Desafio Web #


## Proposta

A SofTeam assinou um contrato para um projeto à pedido de Mineta.

O site será uma galeria de imagens com a funcionalidade parecida ao Stories do Instagram. O site terá as seguintes funcionalidades.

 - Um carrossel finito, que exibe 3 [três] cards na tela principal, e que pode ser iterado, mostrando os próximos ou anteriores (deve ter botão para avançar ou retroceder), deve ter no mínimo 9 cards;

 - Botão de próximos e anteriores (percorrer sempre 1 [um] card) nos extremos (quando possível);

 - Cards devem seguir o seguinte padrão:

     - Nome do usuário;

     - Foto do perfil;

     - Lista com imagens desse usuário.


Ao clicar em um dos cards principais, deve abrir um modal preenchendo a tela com uma imagem da lista deste usuário, um botão para fechar o modal, botão de próximo e anterior (quando possível) e um botão de "curtir" imagem.



Obs.: As informações não serão persistentes, inclusive, as imagens podem ser escolhidas pelo desenvolvedor, para modelar o site, ou utilizar algum servido como o [Lorem Picsun](https://picsum.photos), [Lorempixel](https://lorempixel.com) ou [ImgPlaceholder](https://imgplaceholder.com).

#### Opcional
Adicionar botão para adicionar cards, com:

 - Nome do usuário;

 - Imagem de perfil;

 - Lista de imagens para exibir.

Preferencialmente ao adicionar um novo card deve ser adicionado ao início dos cards


## Instruções

 - Deve ser realizado um clone do repositório [Desafio Web](https://bitbucket.org/netojocelino/treinamento-web)

 - Criar uma pasta com seu nome e ***site-1*** separado por hífen. ***eg:*** _jocelino-neto-site-1_ ***Apenas estes arquivos devem ser modificados***;

 - Deve ser utilizado:

   - *Obrigatoriamente:*

     - HTML5

     - CSS3

     - JavaScript

   - *Opcionalmente:*

      - Bootstrap

      - jQuery



## Prazo

O site tem prazo de 5 dias úteis (15h de desenvolvimento);

## Informações adicionais

Quaisquer dúvidas necessários devem ser enviadas para o email jocelino@softeam.com.br;

Caso o desafio não possa ser realizado deve ser enviado uma justificativa para o email jocelino@softeam.com.br até o dia XX de YY de 2018;

O inicio do desafio será XX de YY de 2018 e finalizado XX de YY de 2018.




Boa sorte!!!
