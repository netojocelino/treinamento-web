clear
rm -rf public/felipe-pereira
rm -rf public/murilo-formiga
rm -rf public/stephane-chagas
rm -rf public/arthur-tavares

git pull origin master

mkdir -p public/felipe-pereira
mkdir -p public/murilo-formiga
mkdir -p public/stephane-chagas
mkdir -p public/arthur-tavares

cp -r felipe-pereira/boilerplate/* public/felipe-pereira
cp -r murilo-formiga/boilerplate/* public/murilo-formiga
cp -r stephane-chagas public/stephane-chagas
cp -r arthur-tavares public/arthur-tavares


firebase deploy
