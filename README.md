## Etapa 1

### Treinamento Geral - JavaScript
 O objetivo desse treinamento é dar uma base de programação utilizando JavaScript para ser aplicado em suas áreas específicas. Dentre os conceitos dados, o foco será em Orientação a Objeto; Controle de Fluxo; Passagem de Dados; Requisições de Dados.

##### O treinamento será dividido em 2h teóricas e 1h prática.
 - Tópicos que serão abordados:
  - Variáveis e Constantes (15min)
    - Tipos em Javascript
    - Operadores
  - Controles de Fluxo (20min)
    - For
    - While
    - If
    - Ternário
  - Funções (20min)
    - Declaração
    - Funções de Primeira Ordem
  - Escopos (15min)
  - Estruturas de dados (30min)
    - Array
    - Objetos
  - Fazer requisição de dados (20min)

### Referências:

Mozilla Developers Network: https://developer.mozilla.org/pt-BR/docs/Aprender/JavaScript

Eloquent Javascript:
https://eloquentjavascript.net/

Informações sobre HTTP Requests:
https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods

Conceitos de Classes e Objetos:
http://www.argonavis.com.br/cursos/java/j100/old/Java3.pdf

Conteúdo gamificado sobre Flexbox:
https://flexboxfroggy.com/

Curso prático de HTML, CSS e JavaScript:
https://learn.freecodecamp.org/


### Recomendações para iniciantes:

Aprender JS no CodeAcademy:
https://www.codecademy.com/learn/introduction-to-javascript

Curso de JavaScript no freeCodeCamp:
https://learn.freecodecamp.org/

Curso na Udemy de JavaScript:
https://www.udemy.com/curso-javascript-ninja/

Playlist no Youtube sobre Lógica de Programação: https://www.youtube.com/watch?v=HXddFUe6VPU&list=PLVzrOYTg7zYDNdLJbnmhPtPcDaNWoxYVT
