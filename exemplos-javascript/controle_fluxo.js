// Exemplos de Controle de Fluxo

const pi = 3.1415;
let raio = 5;

if(raio > 0) {
  // Se a condição for verdadeira (true), esse trecho é executado
  let area = raio * pi * pi;
  console.log('A área do círculo é:' + area);
}
else {
  // Se a condição for falsa (false), esse trecho é executado
  console.log('O raio deve ser positivo');
}



// Multiplicação de a por b

// Declaração das variáveis
let a = 10;
let b = 20;
let multiplicacao = 0;

/*
  Sintaxe de for parecida com C. A primeira parte declara uma variável (Não é obrigado declarar).
  A parte do meio é a condição que deve ser satisfeita para continuar no for.
  A última parte é executada logo após todas as instruções dentro do for serem executadas.
*/
for(let contador = 0; contador < b; contador++ ) {
  multiplicacao = multiplicacao + a;
}
console.log(multiplicacao);


let numero = 1;

/*
  O while será executado enquanto a condição for satisfeita.
  Pra quem não lembra, numero++ é o mesmo que numero = numero + 1.
  O ternário serve para verificar se o número é par ou não (o operador % significa módulo).
*/
while (numero <= 10) {
  numero % 2 == 0 ? console.log('Par') : console.log('Ímpar')
  numero++;
}
