//Exemplo de requisição HTTP

// É um tipo de objeto do JavaScript para requisitar arquivos da internet.
var requisicao = new XMLHttpRequest();

/*
  Essa linha abaixo serve para dizer qual a URL em que será feita a requisição.
  o GET é um tipo de requisição HTTP. Para saber mais sobre tipos de requisições HTTP, acesse o link:
  https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods
*/
requisicao.open("GET", "http://www.json-generator.com/api/json/get/cegxqCHpOW?indent=2", true);

requisicao.onload = function() {
  // Esse if verifica se a requisição teve sucesso ou se houve um erro
  if (requisicao.status >= 200 && requisicao.status < 400) {
    //Esses status são códigos padronizados de sucesso ou falha, para saber mais procure na wiki sobre 'códigos de status de requisição'

    //Tranformando os dados que foram recebidos da requisição em um JSON
    var data = JSON.parse(requisicao.responseText);

    // Imprimindo os dados que foram recebidos da requisição
    console.log("< " + data[0].title + " >");
    console.log("Abra o link " + data[0].front);
    // Como images é um array, podemos passar pelos valores dele usando o forEach
    data[0].images.forEach(function(image){
      console.log("Imagem: " + image);
    });

  } else {
    // Esse else seria executado caso a gente conseguisse fazer a requisição, mas os dados não existiam.

  }
};

requisicao.onerror = function() {
  // Esse código executa quando não é possível se conectar com o servidor da requisição.
};

//Essa é a chamada que envia a requisição de fato.
requisicao.send();
