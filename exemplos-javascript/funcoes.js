
// Funções

  // Para quem não entender o código abaixo, procurem por recursão na internet, é um conceito importante.
  // Declaração de uma função fibonacci que recebe um número 'n'
  function fibonacci(n){
    // Verifica se n é 0 ou 1, se for, retorna o próprio n.
    if( n <= 1 ){
      return n;
    }
    else {
      // Chamada recursiva do fibonacci
      return fibonacci(n-1) + fibonacci(n-2)
    }
  }

  /*
    Outra forma de declarar funções, note que é possível atribuir funções a variáveis,
    isso significa que a variável fibonacci2 contém a função após o igual
    e poderiamos simplesmente escrever fibonacci2(n) e ele chamaria a função abaixo.
    Outra coisa importante é que a função não recebeu um nome, portanto ela é chamada de
    'Anonymous Function'.
  */
  const fibonacci2 = function(n){
    if( n == 0 ) return 0;
    else if ( n == 1 ) return 1;
    else return fibonacci2(n-1) + fibonacci2( n -2 );
  }



  /*
    O código abaixo serve para mostrar que em JavaScript é possível passar funções como parâmetros
    e executar essas funções normalmente.
    Parece complicado de início, mas a ideia é que seja impresso uma mensagem de bem vindo
    com o nome da pessoa. A função chamada 'funcao_primeira_ordem' vai receber o nome da pessoa e
    uma função chamada mensagem. Essa função mensagem é a responsável por imprimir o bem vindo na tela,
    portanto, dentro de 'funcao_primeira_ordem' nós chamamos a função mensagem e passamos o nome recebido. 
  */
  const funcao_primeira_ordem = function(nome, mensagem){
    mensagem(nome);
  };
  const mensagem = function(nome){
    console.log("Bem vindo ao treinamento de JS na SofTeam, " + nome + "!");
  }
  const nome = "Ayres";
  funcao_primeira_ordem(nome, mensagem);
