// Exemplo sobre variáveis e constantes

// Alguns tipos possíveis de variáveis e formas diferentes de declará-las
let nome = "Traineer"; // String
const _numero = 42; // Number
var sono = true; // Bool
let nulo = null;

// typeof(variavel) te diz qual o tipo da variável
console.log(typeof(sono));

// Exemplos de operações que o JavaScript suporta
let outro_numero = _numero + 10;
let novo_nome = nome + " SofTeam";

// Esses exemplos mostram algumas comparações lógicas possível em JavaScript
console.log(_numero == outro_numero);
console.log(_numero <= outro_numero);
console.log(nome != novo_nome);
