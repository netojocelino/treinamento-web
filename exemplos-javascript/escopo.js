// Exemplo de escopo de variáveis

// c é uma variável global, portanto pode ser acessada em qualquer lugar do código
var c = "Oi";

// Declaração de uma função em JavaScript
function numsei() {
  for(let a = 1; a < 10; a++) {
    /*
      A variável 'a' só vale durante o for, após sair do for não é possível acessá-la.
      A variável 'b' vale durante todo escopo de 'numsei', então pode ser usada fora do for.
    */
    var b = "Don't Panic";
  }
  c += ", eu sou o Goku";
  console.log(c);
  console.log(b);
  // A linha abaixo causa um erro
  console.log(a);
}

//Chamando a função declarada acima.
numsei();
