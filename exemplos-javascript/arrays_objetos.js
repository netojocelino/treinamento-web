// Estrutura de dados
  // Array
  //Tipos de declarações de array's
  var   dias_da_semana_numero = [ 0, 1, 2, 3, 4, 5, 6];
  const dias_da_semana_nome   = [ "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"];
  let   jogos_atuais          = ["O JOGO", "Counter Striker 1.6", "Magic The Gathering", "Guilty Gear Xrd -REVELATOR-", "AGAR.IO", "Paladins", "Poker", "FreeFire"];

  /*
    Isso é uma forma de transformar os valores dentro de um array.
    O map é um método que executa o código da função passada como parâmetro
    em todos elementos do array. Nesse caso específico, o map pega cada dia da semana
    e adiciona um texto antes e depois do dia.
    Exemplo: Segunda seria transformado em Hoje é Segunda

    OBS: O map retorna um novo array com os valores modificados, por isso
    que foi declarado 'const dias'
  */
  const dias = dias_da_semana_numero.map( function(dia, posicao){
    return ("Hoje é dia " + dias_da_semana_nome[dia]);
  } );

  /*
    Isso é uma forma passar por todos valores do array e fazer alguma coisa com eles.
    Nesse caso, ele pega cada jogo no array e imprime uma frase dizendo que a gente já
    jogou ele na SofTeam.
    Exemplo: Na SofTeam já jogamos um pouco de Magic The Gathering

    OBS: Diferente do map, ele não retorna um novo array com as modificações, ele muda os
    valores no próprio array (Nesse caso não estamos mudando nada).
  */
  jogos_atuais.forEach(function (jogo, posicao) {
    console.log("Na SofTeam já jogamos um pouco de " + jogo);
  })

  // Objetos

  /*
    Isso é a declaração de uma classe Membro. Ela serve para você dizer quais atributos um membro deve ter,
    como por exemplo nome, endereço, cargo, número de paçocas.
  */
  const Membro = function(nome, cargo) {
    this.nome  = nome;
    this.cargo = cargo;
  };

  /*
    Essa é a sintaxe para definir um método da classe Membro.
    Esse método só pode ser chamado por objetos do tipo Membro.
  */
  Membro.prototype.apresentar = function(){
    console.log( "Meu nome é " + this.nome + " e sou ocupo o cardo cargo de " + this.cargo );
  };

  // Mesma coisa do código acima
  Membro.prototype.empresa = function(){
    console.log( "Oi! Sou o " + this.nome + " e trabalho à base de paçoca e café." );
  };
  /*
    Instanciando objetos do tipo Membro. O nome e cargo foram passados como parâmetros.
    OBS: Note que eles são constantes, portanto não podem ser alterados depois de declarados.
  */
  const men1 = new Membro("Rafael", "Gerente de Sites");
  const women1 = new Membro("Daniele", "Desenvovledora back-end");

  // Chamando os métodos que foram declarados acima
  men1.apresentar();
  women1.empresa();
